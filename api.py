import os

from lifehistory_api import db, app
from lifehistory_api.config import DATABASE_FILE_PATH

if __name__ == '__main__':
    if not os.path.exists(DATABASE_FILE_PATH):
        db.create_all()
    app.run(host='0.0.0.0', threaded=True)
