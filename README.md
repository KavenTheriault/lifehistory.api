# LifeHistory API

Installation
------------

After cloning, create a virtual environment and install the requirements. For Linux and Mac users:

    $ virtualenv venv
    $ source venv/bin/activate
    (venv) $ pip install -r requirements.txt

If you are on Windows, then use the following commands instead:

    $ virtualenv venv
    $ venv\Scripts\activate
    (venv) $ pip install -r requirements.txt

Running
-------

To run the server use the following command:

    (venv) $ python api.py
     * Running on http://127.0.0.1:5000/
     * Restarting with reloader

Then from a different terminal window you can send requests.

# Run in docker container with uwsgi and nginx

The file `db.sqlite` need to be created before generating the image. Unless python is unable to create it in the container.

Create the docker image
```sh
$ docker build -t lifehistoryapi_uwsgi_nginx .
```

Run the image in a new container with local image
```sh
$ docker run -d --name lifehistoryapi -p 5000:80 --mount source=lifehistory_db,target=/app/data lifehistoryapi_uwsgi_nginx
```
Run the image in a new container with git lab registry image
```sh
$ docker run -d --name lifehistoryapi -p 5000:80 --mount source=lifehistory_db,target=/app/data registry.gitlab.com/zoidqc/lifehistory.api
```
Or use docker compose
```sh
$ docker-compose up -d
```

# Deployment notes
Currently, the file `main.py` is not used in production.

#### App folder
`/var/www/life_api_v2/`
```
├── life_api_v2 
│   ├── api.py 
│   ├── requirements.txt 
│   └── lifehistory_api 
│       ├── __init__.py 
│       ├── app.py 
│       ├── config.py 
│       ├── models.py 
│       ├── routes.py 
│       ├── utils.py 
│       └── db.sqlite 
```

#### Wsgi App Ini
`/etc/uwsgi/apps-available/life_api.ini`
```
[uwsgi]
for-readline = /var/www/life_api_v2/.env
  env = %(_)
endfor = 
master=true
module = api
callable = app
chdir = /var/www/life_api_v2/
socket = /tmp/life_api.uwsgi.sock
virtualenv = /var/www/life_api_v2/venv
logto = /var/www/life_api_v2/log/%n.log
chmod-socket = 666
uid = www-data
gid = www-data
```

#### Commands
```
sudo chown -R www-data:www-data /var/www/life_api_v2/

sudo systemctl restart uwsgi-app@life_api.service
sudo systemctl restart nginx.service
```

# Gunicorn
```
gunicorn api:app
```

# 2022 Notes

Database file is not created automatically, we need to add one in the volume manually after starting the container.

**Default volume location**: `/var/lib/docker/volumes/lifehistoryapi_data`