import unittest
from datetime import datetime

from flask import json

from lifehistory_api.models import Day
from tests.factories import DayFactory
from tests.test_base import TestBase, get_authorization_header_for_user


class TestDay(TestBase):
    def test_create_day(self):
        note = 'test_note'
        day = DayFactory(date=datetime(1900, 1, 1), note=note)

        response = self.app.post('/api/days',
                                 headers=get_authorization_header_for_user(1),
                                 data=json.dumps(Day.serialize(day)),
                                 content_type='application/json')

        self.assertEqual(response.status_code, 201)

        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(data['note'], note)

    def test_create_existing_day(self):
        note = 'test_note'
        day = DayFactory(note=note)

        response = self.app.post('/api/days',
                                 headers=get_authorization_header_for_user(1),
                                 data=json.dumps(Day.serialize(day)),
                                 content_type='application/json')

        self.assertEqual(response.status_code, 400)

    def test_get_day(self):
        response = self.app.get('/api/days/1',
                                headers=get_authorization_header_for_user(1))

        self.assertEqual(response.status_code, 200)

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(data['id'], 1)
        self.assertEqual(data['date'], datetime.utcnow().strftime('%Y-%m-%d'))

    def test_get_day_with_date(self):
        response = self.app.get('/api/days/' + datetime.utcnow().strftime('%Y-%m-%d'),
                                headers=get_authorization_header_for_user(1))

        self.assertEqual(response.status_code, 200)

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(data['id'], 1)
        self.assertEqual(data['date'], datetime.utcnow().strftime('%Y-%m-%d'))

    def test_get_day_not_owner(self):
        response = self.app.get('/api/days/1',
                                headers=get_authorization_header_for_user(2))

        self.assertEqual(response.status_code, 401)

    def test_update_day(self):
        note = 'test_update_day'
        day = DayFactory(note=note)

        response = self.app.put('/api/days/1',
                                headers=get_authorization_header_for_user(1),
                                data=json.dumps(Day.serialize(day)),
                                content_type='application/json')

        self.assertEqual(response.status_code, 200)

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(data['id'], 1)
        self.assertEqual(data['note'], note)

    def test_update_day_not_owner(self):
        day = DayFactory(note='test_update_day_not_owner')

        response = self.app.put('/api/days/2',
                                headers=get_authorization_header_for_user(1),
                                data=json.dumps(Day.serialize(day)),
                                content_type='application/json')

        self.assertEqual(response.status_code, 401)


if __name__ == '__main__':
    unittest.main()
