import unittest

from flask import json

from lifehistory_api.models import ActivityType
from tests.factories import ActivityTypeFactory
from tests.test_base import TestBase, get_authorization_header_for_user


class TestActivityType(TestBase):
    def test_create_activity_type(self):
        name = 'test_activity_type'
        activity_type = ActivityTypeFactory(name=name)

        response = self.app.post('/api/activity_types',
                                 headers=get_authorization_header_for_user(1),
                                 data=json.dumps(ActivityType.serialize(activity_type)),
                                 content_type='application/json')

        self.assertEqual(response.status_code, 201)

        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(data['name'], name)

    def test_get_activity_types(self):
        response = self.app.get('/api/activity_types',
                                headers=get_authorization_header_for_user(1))

        self.assertEqual(response.status_code, 200)

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(data[0]['id'], 1)
        self.assertEqual(data[0]['name'], 'activity_type1')

    def test_get_activity_type(self):
        response = self.app.get('/api/activity_types/1',
                                headers=get_authorization_header_for_user(1))

        self.assertEqual(response.status_code, 200)

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(data['id'], 1)
        self.assertEqual(data['name'], 'activity_type1')

    def test_get_activity_type_not_owner(self):
        response = self.app.get('/api/activity_types/1',
                                headers=get_authorization_header_for_user(2))

        self.assertEqual(response.status_code, 401)

    def test_update_activity_types(self):
        name = 'test_update_activity_type'
        activity_type = ActivityTypeFactory(name=name)

        response = self.app.put('/api/activity_types/1',
                                headers=get_authorization_header_for_user(1),
                                data=json.dumps(ActivityType.serialize(activity_type)),
                                content_type='application/json')

        self.assertEqual(response.status_code, 200)

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(data['id'], 1)
        self.assertEqual(data['name'], name)

    def test_update_activity_types_not_owner(self):
        activity_type = ActivityTypeFactory(name='test_update_activity_types_not_owner')

        response = self.app.put('/api/activity_types/2',
                                headers=get_authorization_header_for_user(1),
                                data=json.dumps(ActivityType.serialize(activity_type)),
                                content_type='application/json')

        self.assertEqual(response.status_code, 401)

    def test_delete_activity_types(self):
        response = self.app.delete('/api/activity_types/3',
                                   headers=get_authorization_header_for_user(1))

        self.assertEqual(response.status_code, 200)

    def test_delete_activity_types_not_owner(self):
        response = self.app.delete('/api/activity_types/2',
                                   headers=get_authorization_header_for_user(1))

        self.assertEqual(response.status_code, 401)

    def test_search_activity_types(self):
        response = self.app.get('/api/activity_types/search/activity_type',
                                headers=get_authorization_header_for_user(1))

        self.assertEqual(response.status_code, 200)

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(len(data), 2)
        self.assertEqual(data[0]['name'], 'activity_type1')

    def test_search_activity_types2(self):
        response = self.app.get('/api/activity_types/search/test',
                                headers=get_authorization_header_for_user(1))

        self.assertEqual(response.status_code, 200)

        data = json.loads(response.get_data(as_text=True))

        self.assertEqual(len(data), 0)


if __name__ == '__main__':
    unittest.main()
