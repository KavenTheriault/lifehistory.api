from setuptools import setup

setup(
    name='LifeHistory Api',
    packages=['lifehistory_api'],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask',
    ],
    setup_requires=[
        'pytest-runner',
    ],
    tests_require=[
        'pytest',
    ],
)