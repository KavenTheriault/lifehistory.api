from flask import g
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
from passlib.apps import custom_app_context as pwd_context

from lifehistory_api.app import db, app
from lifehistory_api.utils import get_time_string, get_date_string


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), index=True)
    password_hash = db.Column(db.String(64))
    created_date = db.Column(db.DateTime, nullable=False)
    name = db.Column(db.String(128), nullable=False)

    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    def generate_auth_token(self, expiration=600):
        s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None
        except BadSignature:
            return None
        user = User.query.get(data['id'])
        return user

    @staticmethod
    def verify_user_and_password(username_or_token, password):
        user = User.verify_auth_token(username_or_token)
        if not user:
            user = User.query.filter_by(username=username_or_token).first()
            if not user or not user.verify_password(password):
                return False
        g.user = user
        return True


class ActivityType(db.Model):
    __tablename__ = 'activity_types'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    created_date = db.Column(db.DateTime, nullable=False)
    name = db.Column(db.String(128), nullable=False)
    show_quantity = db.Column(db.Boolean, nullable=False)
    show_rating = db.Column(db.Boolean, nullable=False)

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'show_quantity': self.show_quantity,
            'show_rating': self.show_rating,
        }


class Activity(db.Model):
    __tablename__ = 'activities'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    created_date = db.Column(db.DateTime, nullable=False)
    name = db.Column(db.String(128), nullable=False)
    activity_type_id = db.Column(db.Integer, db.ForeignKey('activity_types.id'), nullable=False)
    activity_type = db.relationship('ActivityType', backref=db.backref('activities', lazy='dynamic'))

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'activity_type_id': self.activity_type_id,
            'activity_type': ActivityType.serialize(self.activity_type),
        }


class Day(db.Model):
    __tablename__ = 'days'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    created_date = db.Column(db.DateTime, nullable=False)
    date = db.Column(db.DateTime, nullable=False)
    note = db.Column(db.String(4096))
    life_entries = db.relationship('LifeEntry', backref='days', lazy='dynamic')

    def serialize(self):
        return {
            'id': self.id,
            'date': get_date_string(self.date),
            'note': self.note,
            'life_entries': [LifeEntry.serialize(life_entry) for life_entry in self.life_entries],
        }


class LifeEntry(db.Model):
    __tablename__ = 'life_entries'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    created_date = db.Column(db.DateTime, nullable=False)
    day_id = db.Column(db.Integer, db.ForeignKey('days.id'), nullable=False)
    start_time = db.Column(db.Time, nullable=False)
    end_time = db.Column(db.Time)
    life_entry_activities = db.relationship('LifeEntryActivity', backref='life_entries', lazy='dynamic')

    def serialize(self):
        return {
            'id': self.id,
            'day_id': self.day_id,
            'start_time': get_time_string(self.start_time),
            'end_time': get_time_string(self.end_time),
            'life_entry_activities': [LifeEntryActivity.serialize(life_entry_activity) for life_entry_activity in
                                      self.life_entry_activities],
        }


class LifeEntryActivity(db.Model):
    __tablename__ = 'life_entry_activities'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    created_date = db.Column(db.DateTime, nullable=False)
    life_entry_id = db.Column(db.Integer, db.ForeignKey('life_entries.id'), nullable=False)
    activity_id = db.Column(db.Integer, db.ForeignKey('activities.id'), nullable=False)
    description = db.Column(db.String(512))
    quantity = db.Column(db.Float)
    rating = db.Column(db.Integer)
    activity = db.relationship('Activity', backref=db.backref('life_entry_activities', lazy='dynamic'))

    def serialize(self):
        return {
            'id': self.id,
            'life_entry_id': self.life_entry_id,
            'description': self.description,
            'quantity': self.quantity,
            'rating': self.rating,
            'activity_id': self.activity_id,
            'activity': Activity.serialize(self.activity),
        }
